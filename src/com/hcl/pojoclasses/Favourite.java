package com.hcl.pojoclasses;

public class Favourite {

	   private int id;
	   private  String title;
	   private  String releaseDate;
	   private int year;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	@Override
	public String toString() {
		return "Favourite [id=" + id + ", title=" + title + ", releaseDate=" + releaseDate + ", year=" + year + "]";
	}
	   
			
}
