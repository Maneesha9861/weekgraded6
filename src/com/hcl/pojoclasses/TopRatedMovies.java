package com.greatlearning.week6.bean;

public class TopRatedMovies {

	    private  String title;
		private  String releaseDate;
		private String imbdRating;
		private String Duration;
		private int year;
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getReleaseDate() {
			return releaseDate;
		}
		public void setReleaseDate(String releaseDate) {
			this.releaseDate = releaseDate;
		}
		public String getImbdRating() {
			return imbdRating;
		}
		public void setImbdRating(String imbdRating) {
			this.imbdRating = imbdRating;
		}
		public String getDuration() {
			return Duration;
		}
		public void setDuration(String duration) {
			Duration = duration;
		}
		public int getYear() {
			return year;
		}
		public void setYear(int year) {
			this.year = year;
		}
		@Override
		public String toString() {
			return "TopRatedMovies [title=" + title + ", releaseDate=" + releaseDate + ", imbdRating=" + imbdRating
					+ ", Duration=" + Duration + ", year=" + year + "]";
		}
	
		
}

