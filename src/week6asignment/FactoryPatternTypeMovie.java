package week6asignment;

public class FactoryPatternTypeMovie {

	public IMovies getMovies(String movieType) {

		if (movieType == null) {
			return null;
		}
		if (movieType.equalsIgnoreCase("MOVIES_COMMING")) {
			return new MoviesComingSoon();
		}
		else if (movieType.equalsIgnoreCase("MOVIES_IN_THEATRES")) {
			return new TheatricalRelease();
		} 
		else if (movieType.equalsIgnoreCase("TOP_RATED_INDIA")) {
			return new TopRatedMoviesInIndia();
		} 
		else if (movieType.equalsIgnoreCase("TOP_RATED_MOVIES")) {
			return new TopRatedMovies();
		}
		return null;
	}

}